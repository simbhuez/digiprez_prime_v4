import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators  } from '@angular/forms';

import { GlobalService} from '../../services/global/global.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.page.html',
  styleUrls: ['./resetpassword.page.scss'],
})
export class ResetpasswordPage implements OnInit {
  
  public resetYourPassword: FormGroup;

  constructor(public formBuilder: FormBuilder,
              public global: GlobalService) {
    this.resetYourPassword = this.formBuilder.group({
      emailId: ['', Validators.required]
    })

  }

  ngOnInit() {
  }

  resetPassword() {
    
  }
}
