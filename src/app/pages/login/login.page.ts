import { Component, OnInit } from '@angular/core';

import { NavController } from '@ionic/angular'

import { GlobalService} from '../../services/global/global.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public userLoginDetails: any = {
    loginId: "test",
    password:"test"
  }
  public disableBtnInd: boolean = true;
  public passwordType: any = 'password';
  public displayEyeInd: boolean = false;

  constructor(public navCtrl: NavController,
              public global: GlobalService) {
  }

  ngOnInit() {
  }

  login() {
    if(this.userLoginDetails.loginId != '' && this.userLoginDetails.password != '') {
      console.log(this.userLoginDetails);
      this.navCtrl.navigateForward('tabs');
    } else {
      this.global.presentAlert();
      
    }
  }

  displayPassword(eyeInd) {
    if(eyeInd == 'eyeOff') {
      this.passwordType = 'text';
      this.displayEyeInd = true;
    } else {
      this.passwordType = 'password';
      this.displayEyeInd = false;
    }
  }

  openRegisterPage() {
    this.navCtrl.navigateForward('register');
  }

  openResetPasswordPage() {
    this.navCtrl.navigateForward('resetpassword');
  }

}
