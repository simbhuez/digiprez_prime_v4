import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { GlobalService} from '../../services/global/global.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  public registerForm: FormGroup;

  constructor(public formBuilder: FormBuilder,
              public global: GlobalService) {
    this.registerForm = this.formBuilder.group({
      registrationNumber: ['', Validators.required],
      registrationState: ['', Validators.compose([Validators.pattern('[a-zA-Z]*'),Validators.required])],
      name: ['', Validators.required],
      fatherName: ['', Validators.required],
      qualification: ['', Validators.required],
      qualificationYear: ['', Validators.required],
      speciality: ['', Validators.required],
      mobileNumber: ['', Validators.required],
      emailAddress: ['', Validators.required],
      pincode: ['', Validators.compose([Validators.maxLength(6) ,Validators.required])]
    });

    console.log(this.global.appDate);
  }

  ngOnInit() {
  }

}
