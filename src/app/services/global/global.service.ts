import { Injectable } from '@angular/core';

import { LoadingController, AlertController, ModalController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  public appDate = new Date();
  isLoading = false;

  constructor(public loadingController: LoadingController,
              public alertController: AlertController,
              public modelController: ModalController) { 

  }
  //present/dismissing loading function
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait loading...',
      backdropDismiss: false
    });
    await loading.present();
  }

  async dismissLoading() {
    console.log('dismiss');
    await this.modelController.dismiss().then(() => {
      console.log('1');
    }).catch((error) => {
      console.log(error);
    })
  }

  //present alert function
  async presentAlert() {
    const alert = await this.alertController.create({
      message: 'Invalid email id and/or password',
      buttons: ['Ok']
    });
    await alert.present();
  }

}
